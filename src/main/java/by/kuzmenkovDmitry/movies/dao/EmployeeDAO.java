package by.kuzmenkovDmitry.movies.dao;

import by.kuzmenkovDmitry.movies.model.Employee;

import java.util.List;

/**
 * Created by user on 19.07.2016.
 */
public interface EmployeeDAO {
    List<Employee> getAllEmployee();
    Employee getEmployee(int id);
    void updateEmployee(Employee employee);
    void deleteEmployee(int id);
    void createEmployee(Employee employee);
    List<Employee> getEmployeeToPage(int numberPage);
}
