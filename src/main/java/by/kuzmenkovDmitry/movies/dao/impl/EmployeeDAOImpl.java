package by.kuzmenkovDmitry.movies.dao.impl;

import by.kuzmenkovDmitry.movies.dao.EmployeeDAO;
import by.kuzmenkovDmitry.movies.model.Employee;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
public class EmployeeDAOImpl implements EmployeeDAO {

    private EntityManager entityManager;

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public List<Employee> getAllEmployee() {
        return  entityManager.createQuery("select e from Employee e").getResultList();
    }

    public Employee getEmployee(int id) {
        return entityManager.createQuery("select e from Employee e where e.id = :id", Employee.class).setParameter("id", id).getSingleResult();
    }

    public void updateEmployee(Employee employee) {
        entityManager.merge(employee);
    }

    public void deleteEmployee(int id) {
        entityManager.remove(getEmployee(id));
    }

    public void createEmployee(Employee employee) {
        entityManager.merge(employee);
    }

    public List<Employee> getEmployeeToPage(int numberPage) {
        return entityManager.createQuery("select e from Employee e").setFirstResult((numberPage-1)*5).setMaxResults(5).getResultList();
    }
}
