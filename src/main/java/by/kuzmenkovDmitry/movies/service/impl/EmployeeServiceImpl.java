package by.kuzmenkovDmitry.movies.service.impl;

import by.kuzmenkovDmitry.movies.dao.EmployeeDAO;
import by.kuzmenkovDmitry.movies.model.Employee;
import by.kuzmenkovDmitry.movies.service.EmployeeService;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.List;

@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService {

    private EmployeeDAO employeeDAO;

    @Inject
    public void setEmployeeDAO(EmployeeDAO employeeDAO) {
        this.employeeDAO = employeeDAO;
    }

    public List<Employee> getAllEmployee() {
        return employeeDAO.getAllEmployee();
    }

    public Employee getEmployee(int id) {
        return employeeDAO.getEmployee(id);
    }

    public void updateEmployee(Employee employee) {
        employeeDAO.updateEmployee(employee);
    }

    public void deleteEmployee(int id) {
        employeeDAO.deleteEmployee(id);
    }

    public void createEmployee(Employee employee) {
        employeeDAO.createEmployee(employee);
    }

    public List<Employee> getEmployeeToPage(int numberPage) {
        return employeeDAO.getEmployeeToPage(numberPage);
    }
}
